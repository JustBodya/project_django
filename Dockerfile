FROM python:3.10-alpine

WORKDIR /app

COPY ./app .
COPY ./requirements.txt .
COPY ./run.sh .
COPY ./nginx.conf .

RUN pip install -r requirements.txt

ENV PYTHONUNBUFFERED=1

CMD ["sh", "run.sh"]